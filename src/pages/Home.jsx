import { Banner } from '../components/navbar/Banner';
import { Buscador } from '../components/buscador/Buscador';
import { ListadoPersonajes } from '../components/Personajes/ListadoPersonajes';
import { useState } from 'react';

export function Home() {
  let [buscador, setBuscador] = useState('');
  console.log(buscador);
  return (
    <div>
      <Banner />
      <div className='px-5'>
        <h1 className='py-4'>Personajes Destacados</h1>

        <Buscador valor={buscador} onBuscar={setBuscador} />
        <ListadoPersonajes buscar={buscador} />
      </div>
    </div>
  );
}
