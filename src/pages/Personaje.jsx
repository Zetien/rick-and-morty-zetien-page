import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { BannerDetallePersonaje } from '../components/Personajes/BannerDetallePersonaje';
import { ListaEpisodios } from '../components/episodio/ListaEpisodios';

export function Personaje() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionesEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });
      Promise.all(peticionesEpisodios).then((respuestas) => {
        setEpisodios(respuestas);
      });
    }
  }, [personaje]);

  return (
    <div className='p-5'>
      {personaje ? (
        <div>
          <BannerDetallePersonaje {...personaje} />
          <h2 className='py-4'>Episodios</h2>
          {episodios ? (
            <ListaEpisodios episodios={episodios} />
          ) : (
            <div>Cargando episodios</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}
