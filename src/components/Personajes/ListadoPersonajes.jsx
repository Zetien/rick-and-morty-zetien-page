import { PersonajeItem } from './PersonajeItem';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Pagination from 'react-js-pagination';

export function ListadoPersonajes({ buscar }) {
  const [pagina, setPagina] = useState(1);
  const [personajes, setPersonajes] = useState(null);
  let personajesFiltrados = personajes?.results;

  let URL = 'https://rickandmortyapi.com/api/character';
  let LIMIT = 20;

  function handlePageChange(nuevaPag) {
    setPagina(nuevaPag);
  }

  if (buscar && personajes) {
    personajesFiltrados = personajes.results.filter((personaje) => {
      let nombrePersonajeMinuscula = personaje.name.toLowerCase();
      let buscadorMinuscula = buscar.toLowerCase();
      return nombrePersonajeMinuscula.includes(buscadorMinuscula);
    });
  }
  console.log(personajesFiltrados);
  useEffect(() => {
    axios.get(`${URL}?page=${pagina}`).then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, [pagina]);

  return (
    <div className='row py-5'>
      {personajesFiltrados
        ? personajesFiltrados.map((elemento) => {
            return <PersonajeItem key={elemento.id} {...elemento} />;
          })
        : 'cargando...'}
      <div className='d-flex justify-content-center'>
        <Pagination
          itemClass='page-item'
          linkClass='page-link'
          activePage={pagina}
          itemsCountPerPage={LIMIT}
          totalItemsCount={826}
          onChange={handlePageChange}
        />
      </div>
    </div>
  );
}
