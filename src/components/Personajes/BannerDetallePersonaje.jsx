function getEstilosStatus(status) {
  let color = 'green';
  if (status === 'unknown') {
    color = 'gray';
  }
  if (status === 'Dead') {
    color = 'red';
  }
  const circuloEstado = {
    width: '10px',
    height: '10px',
    display: 'inline-block',
    backgroundColor: color,
    borderRadius: '50%',
    marginRight: '5px',
  };
  return circuloEstado;
}
export function BannerDetallePersonaje({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  return (
    <div className='card mb-3 bg-secondary'>
      <div className='row g-0'>
        <div className='col-md-3'>
          <img
            style={{ height: '100%', objectFit: 'cover' }}
            src={image}
            className='img-fluid rounded-start'
            alt={name}
          />
        </div>
        <div className='col-md-8'>
          <div className='card-body'>
            <h5 className='card-title mb-0 text-white'>{name}</h5>
            <p className='text-white'>
              <span style={getEstilosStatus(status)}></span>
              {status} - {species}
            </p>
            <p className='mb-0 text-white'>Last Know Location:</p>
            <p className='text-white'>{location?.name}</p>
            <p className='mb-0 text-white'>First seen in:</p>
            <p className='text-white'>{origin?.name}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
