import { Link } from 'react-router-dom';
function getEstilosStatus(status) {
  let color = 'green';
  if (status === 'unknown') {
    color = 'gray';
  }
  if (status === 'Dead') {
    color = 'red';
  }
  const circuloEstado = {
    width: '10px',
    height: '10px',
    display: 'inline-block',
    backgroundColor: color,
    borderRadius: '50%',
    marginRight: '5px',
  };
  return circuloEstado;
}

export function PersonajeItem({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  return (
    <div className='col-sm-6 col-lg-4 col-xl-3'>
      <Link
        to={`/Personaje/${id}`}
        style={{ textDecoration: 'none', color: 'initial' }}
      >
        <div className='card mb-3 bg-secondary' style={{ maxWidth: '540px' }}>
          <div className='row g-0'>
            <div className='col-md-4'>
              <img
                style={{ height: '100%', objectFit: 'cover' }}
                src={image}
                className='img-fluid rounded-start'
                alt='...'
              />
            </div>
            <div className='col-md-8'>
              <div className='card-body'>
                <h5 className='card-title mb-0 text-white'>{name}</h5>
                <p className='text-white'>
                  <span style={getEstilosStatus(status)}></span>
                  {status} - {species}
                </p>
                <p className='mb-0 text-white'>Last Know Location:</p>
                <p className='text-white'>{location?.name}</p>
                <p className='mb-0 text-white'>First seen in:</p>
                <p className='text-white'>{origin?.name}</p>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}
