import bannerImg from '../../assets/img/ramBanner.jpeg';
export function Banner() {
  return (
    <div>
      <img
        style={{ height: '100%', objectFit: 'cover' }}
        src={bannerImg}
        className='card-img'
        alt='banner'
      />
    </div>
  );
}
