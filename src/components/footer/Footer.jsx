export function Footer() {
  return (
    <div className='navbar-success bg-success fs-5 fixed-bottom'>
      <p className='text-center text-white py-2 mb-0'>
        Proyecto creado para el CAR IV Clase React Basico 14-02-2022 by Zetien
      </p>
    </div>
  );
}
