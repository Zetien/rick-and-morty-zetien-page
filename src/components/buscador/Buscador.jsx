export function Buscador({ valor, onBuscar }) {
  return (
    <div className='d-flex justify-content-end'>
      <div className='mb-3 col-5'>
        <label className='form-label'></label>
        <input
          type='text'
          className='form-control'
          placeholder='Buscar Personajes...'
          value={valor}
          onChange={(evento) => onBuscar(evento.target.value)}
        />
      </div>
    </div>
  );
}
